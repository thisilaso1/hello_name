pub fn hello_user(name: &str) -> String {
    let mut owned_string :String = "hello ".to_owned();
    owned_string.push_str(name);
    owned_string
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result =  hello_user("nqhung");
        assert_eq!(result, "hello nqhung");
    }
}
